package javafxpoo1;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.FileWriter; 

public class JSONWrite {

    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException{
        JSONObject obj = new JSONObject();
		obj.put("nombre", "Spyro");
		obj.put("edad", new Integer(32));
	 
		JSONArray list = new JSONArray();
		list.add("Java");
		list.add("Ceylon");
		list.add("Python");
	 
		obj.put("lenguajes_favoritos", list);
		
		try{
			FileWriter file = new FileWriter("/src/temp/test.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();
			
			
		}catch(Exception ex){
			System.out.println("Error: "+ex.toString());
		}
		finally{
			System.out.print(obj);
		}
    }

}


