/*------------------------------------------------------------------\*
Instituto Tecnologico de Costa Rica
Curso: Programacion Orientada-Objetos
Integrantes: Andres Lopez/Fabian Lopez/Steven Alvarado
Semestre: II
Porfesora: Samanta Ramijan 
*/
package javafxpoo1;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class JavaFXpoo1 extends Application  {
    Scene scene;
            
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXML.fxml"));
        
         scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();     
    }

    public static void main(String[] args) {
        launch(args);
      }
    }