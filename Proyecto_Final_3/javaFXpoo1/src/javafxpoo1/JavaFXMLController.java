/*------------------------------------------------------------------\*
Instituto Tecnologico de Costa Rica
Curso: Programacion Orientada-Objetos
Integrantes: Andres Lopez/Fabian Lopez/Steven Alvarado
Semestre: II
Porfesora: Samanta Ramijan 
*/
package javafxpoo1;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class JavaFXMLController extends FXMLController1 implements Initializable  {
@FXML
    public Button button;
@FXML
    public ProgressBar pbAcariciar;
@FXML
    public ProgressBar pbSalud;
@FXML
    public ProgressBar pbLimpiar;
@FXML
    public ProgressBar pbAlimentar;
@FXML
    public ImageView imagen;
@FXML
    private TextField TXfecha;
@FXML
    private TextField TXpuntaje;
    
    public int puntaje;
    double num = 1;
    double num2 = 1;
    double num3 = 0;
    double num4 = 0;
    String points;
    
    private Thread dateThread;
    private DateFormat formato = new SimpleDateFormat("hh:mm:ss");

    boolean isRunning = true;
    private final StringProperty twoWayInput = new SimpleStringProperty("");


            
 
 @FXML
 /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Subir la felicidad e la mascota
    -----------------------------------------------------------------\*
*/
 public void buttonAcariciar(ActionEvent event) {
    if(pbAcariciar.getProgress()==1){
       num = 1;
    }
    else{
        num=num+0.2;
        pbAcariciar.setProgress(num);
   }
 }
 /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Subir la salud de la mascota
    -----------------------------------------------------------------\*
*/
 public void buttonCurar(ActionEvent event){
    if(pbSalud.getProgress()==1){
       num2 = 1;
    }
    else{
        num2=num2+0.2;
        pbSalud.setProgress(num2);
    }
 }
 /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Bajar la suciedad de la mascota
    -----------------------------------------------------------------\*
*/
  public void buttonLimpiar(ActionEvent event){
      if(pbLimpiar.getProgress()==0){
         num3 = 0;
    }
    else{
        num3=num3-0.2;
        pbLimpiar.setProgress(num3);
    }
  }
  /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Bajar el hambre de la mascota
    -----------------------------------------------------------------\*
*/
  public void buttonAlimentar(ActionEvent event){
    if(pbAlimentar.getProgress()==0){
       num4 = 0;
    }
    else{
        num4=num4-0.2;
        pbAlimentar.setProgress(num4);
    }
  }
  /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Modificar el puntaje 
    -----------------------------------------------------------------\*
*/
    public void puntaje(){
     TimerTask task = new TimerTask() {
      @Override
      public void run() {
          if (pbAcariciar.getProgress()>0.6){
              puntaje = puntaje +10;
          }
             else{
          puntaje =puntaje-10;
          }
          if(pbSalud.getProgress()==1){
              puntaje =puntaje+10;
          }
          if(pbSalud.getProgress()>0&&pbSalud.getProgress()>=0.4){
              puntaje = puntaje-10;
          }
          if(pbLimpiar.getProgress()>=0.8){
              puntaje =puntaje-10;
          }
          if (pbAlimentar.getProgress()<4){
              puntaje = puntaje+10;
          }
          points =String.valueOf(puntaje);
          TXpuntaje.setText(points);

      }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 10000; 
    // schedules the task to be run in an interval 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
  } // end of main
        
    @Override
    public void initialize(URL location, ResourceBundle resources){
        
        TXfecha.textProperty().bindBidirectional(twoWayInputProperty());
        dateThread = new Thread(this::UsarHilo);
        dateThread.start();
        bajarSalud();
        bajarFelicidad();
        subirSuciedad();
        subirHambre();
        puntaje();
        muerto();
    }
/*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Utilizar el hilo
    -----------------------------------------------------------------\*
*/
private void UsarHilo(){
    while (isRunning){
        
        String dateStr = ("La hora es: " + formato.format(new Date()));
         //Cambia a hilo de Interfaz
        Platform.runLater(() -> {setTwoWayInput(dateStr);});
        try{
            Thread.sleep(2000);
            } 
        catch (InterruptedException iex)
            {

        }
    }
}

    public StringProperty twoWayInputProperty(){
        return twoWayInput;
    }

    public void setTwoWayInput(String twoWayInput){
        this.twoWayInput.set(twoWayInput);
    }
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Bajar la salud
    -----------------------------------------------------------------\*
*/
    public void bajarSalud(){
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        if(pbSalud.getProgress()<0.1){
            
        }
        else{
            num2 = num2-0.2;
         pbSalud.setProgress(num2);
        }
      }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 9000; 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
  } 
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Baja la felicidad
    -----------------------------------------------------------------\*
*/
    public void bajarFelicidad(){
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        if(pbAcariciar.getProgress()==0){
        }
        else{
        num = num-0.2;
        pbAcariciar.setProgress(num);
        }
      }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 9000; 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
  } 
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Aumenta la suciedad
    -----------------------------------------------------------------\*
*/
    public void subirSuciedad(){
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        if(pbLimpiar.getProgress()==1){       
        }
        else{
        num3 = num3+0.2;
        pbLimpiar.setProgress(num3);
        }
      }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 9000; 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
 
}
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Aumenta el hambre
    -----------------------------------------------------------------\*
*/
    public void subirHambre(){
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        if (pbAlimentar.getProgress()==1){
        
        }
        else{
        num4 = num4+0.2;
        pbAlimentar.setProgress(num4);
        }
      }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 9000; 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
 
}
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Se muere la mascota y termina el juego
    -----------------------------------------------------------------\*
*/
    public void muerto(){
      TimerTask task = new TimerTask() {
      
      @Override
      public void run() {   
      if(pbSalud.getProgress()<0.1){
         TimerTask task = new TimerTask() {
        @Override
         public void run() {
             
        if(pbSalud.getProgress()<0.1){
          System.out.println("Su mascota se murio");
          System.out.println("GAME OVER!");
          System.exit(1);
        }
      }
        };
        Timer timer = new Timer();
         long delay = 10000;
         long intevalPeriod = 1 * 10000; 
        timer.scheduleAtFixedRate(task, delay,intevalPeriod);
        }
      else{
      if(pbSalud.getProgress()<0.1&&pbLimpiar.getProgress()==1){
          System.out.println("Su mascota se murio");
          System.out.println("GAME OVER!");
          System.exit(1);
      }
      }
        }
    };
    Timer timer = new Timer();
    long delay = 0;
    long intevalPeriod = 1 * 1000; 
    timer.scheduleAtFixedRate(task, delay,intevalPeriod);
 
        
    }
    /*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Vuelve a la escena principal
    -----------------------------------------------------------------\*
*/
    public void volver(ActionEvent event) throws IOException, InterruptedException{

        Parent root = FXMLLoader.load(getClass().getResource("FXML.fxml"));
        scene =  new Scene(root);
        
       Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
