package javafxpoo1;

import java.util.*;

public class Relog implements Runnable {
    
    String hora, minutos, segundos, ampm;
    Calendar calendario;
    Thread h1;
    
    public Relog(){

        initComponents();
        h1 = new Thread(this);
        h1.start();

    }

    private void initComponents() {
    }

    @Override
    public void run() {
        Thread rl = Thread.currentThread();

        while (rl == h1){
            calcula();
            System.out.println(hora+":"+minutos+":"+segundos+":"+" "+ampm);
            try{
                Thread.sleep(5000);
            }catch (InterruptedException e){}
        }
    }

    private void calcula() {
        Calendar calendario = new GregorianCalendar();
        //Date fechaHoraActual = new Date();
        //calendario.setTime(fechaHoraActual);
        ampm = calendario.get(Calendar.AM_PM) == Calendar.AM?"AM":"PM";

        /*Aca verificamos si es am o pm, si es pm restamos 12 horas a esa hora, y si la hora es menor a 9
        agregamos un cero para que se cumpla el formato de la hora*/
        if(ampm.equals("PM")){
            int h1 = calendario.get(Calendar.HOUR)-12;
            hora = h1 > 9?""+h1:"0"+h1;
        }else{
            hora = calendario.get(Calendar.HOUR) > 9?""+calendario.get(Calendar.HOUR):"0"+calendario.get(Calendar.HOUR);
        }
        minutos = calendario.get(Calendar.MINUTE) > 9?""+calendario.get(Calendar.MINUTE):"0"+calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND) > 9?""+calendario.get(Calendar.SECOND):"0"+calendario.get(Calendar.SECOND);
    }

}
