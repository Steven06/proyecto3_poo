/*------------------------------------------------------------------\*
Instituto Tecnologico de Costa Rica
Curso: Programacion Orientada-Objetos
Integrantes: Andres Lopez/Fabian Lopez/Steven Alvarado
Semestre: II
Porfesora: Samanta Ramijan 
*/
package javafxpoo1;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXMLController1 extends JavaFXpoo1 implements Initializable  {
    Scene sceneSpyro;
    Scene sceneGato;
    Scene mainScene = scene;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
     
    }
/*------------------------------------------------------------------\*
    Entradas:NULL
    Salidas:NULL
    Restricciones:NULL
    Funcion:Crear una escena 
    -----------------------------------------------------------------\*
*/
   public void buttonSpyro(ActionEvent event) throws IOException{
       Parent parent2 = FXMLLoader.load(getClass().getResource("javaFXML.fxml"));
       sceneSpyro = new Scene(parent2);
       Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

       window.setScene(sceneSpyro);
       window.show(); 
    }
/*------------------------------------------------------------------\*
    Entradas:Null
    Salidas:NULL
    Restricciones:NULL
    Funcion:Crear una escena
    -----------------------------------------------------------------\*
*/
    public void buttonGato(ActionEvent event) throws IOException{
       Parent parent2 = FXMLLoader.load(getClass().getResource("FXML2.fxml"));
       sceneGato = new Scene(parent2);
       Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

       window.setScene(sceneGato);
       window.show();
    }
    
   
}