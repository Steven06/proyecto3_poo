/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxpoo1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Steven Alvarado
 */
public class JSONRead {
    
    	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException{
		
	
		JSONParser parser = new JSONParser();
		try{
			Object obj = parser.parse(new FileReader("/src/test.json"));
			 
			JSONObject jsonObject = (JSONObject) obj;
	 
			String nombre = (String) jsonObject.get("nombre");
			System.out.println("nombre:"+nombre);
	 
			long edad = (Long) jsonObject.get("edad");
			System.out.println("edad:"+edad);
	 
			// recorrer arreglo
			JSONArray leng= (JSONArray) jsonObject.get("lenguajes_favoritos");
			System.out.println("lenguajes_favoritos:");
			Iterator iterator =leng.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			
		}catch(Exception ex){
			System.err.println("Error: "+ex.toString());
		}finally{
			
		}
}
    
}
